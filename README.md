# DSC Summer Camp for cloud

## 8/11

### Before install

```
> sudo apt update && sudo apt install -y git
> git clone https://gitlab.com/coosos810609/dsc-summer-camp.git
```

## 8/12

這個教材為 2020/8/12 提供給 DSC Summer Camp 的學生所使用的參考教材，學生可以透過這個教材來實做出課程內容。

### Example 1

一個簡單的 PHP 專案，可以直接透過 `gcloud app deploy` 將他部署上 App Engine 上，這個教材是使用標準模式來進行部署。

### Example 2

一個簡單的 golang 專案，並且有一個可以執行此專案的 Docker file ，可以直接透過 `gcloud app deploy` 將這個專案 deploy 上 App Engine ，這個教材是使用彈性模式進行部署。

### Example 3

還是一樣是個簡單的 PHP 專案，但主要的差異是多使用了 compose 這個套件管理工具，並且使用 PHPUnit 來進行專案單元測試，透過 Gitlab CI 來進行專案驗證，並且自動部署專案到 App Engine 上。

若教材有任何問題，歡迎開 Issue 來進行提問。
